var url_string = window.location.href;
var url = new URL(url_string);

// Defino el Año
if (url.searchParams.get("year")){
	var year = parseInt(url.searchParams.get("year"))
}else{
	var year = 2060
}

// Defino el Mes
if (url.searchParams.get("month")){
	var month = parseInt(url.searchParams.get("month"))
}else{
	var month = 03
}

// Defino el Dia
if (url.searchParams.get("day")){
	var day = parseInt(url.searchParams.get("day"))
}else{
	var day = 29
}

// Defino la Hora
if (url.searchParams.get("hour")){
	var hour = parseInt(url.searchParams.get("hour"))
}else{
	var hour = 09
}

// Defino los Minutos
if (url.searchParams.get("minutes")){
	var minutes = parseInt(url.searchParams.get("minutes"))
}else{
	var minutes = 15
}


lynxCountdown('#cuenta', {
	year: year, // required
	month: month, // required
	day: day, // required
	hours: hour, // Default is 0 [0-23] integer
	minutes: minutes, // Default is 0 [0-59] integer
	seconds: 0, // Default is 0 [0-59] integer
	words: { //words displayed into the countdown
		days: 'Día',
		hours: 'Hora',
		minutes: 'Minuto',
		seconds: 'Segundo',
		pluralLetter: 's'
	},
	plural: true, //use plurals
	inline: false, //set to true to get an inline basic countdown like : 24 days, 4 hours, 2 minutes, 5 seconds
	inlineClass: 'lynx-countdown-inline', //inline css span class in case of inline = true
	// in case of inline set to false
	enableUtc: true, //Use UTC as default
	onEnd: function() {
		document.getElementById('portada').classList.add('oculta');
		return;
	}, //Callback on countdown end, put your own function here
	refresh: 1000, // default refresh every 1s
	sectionClass: 'lynx-section', //section css class
	amountClass: 'lynx-amount', // amount css class
	wordClass: 'lynx-word', // word css class
	zeroPad: false,
	countUp: false
});
