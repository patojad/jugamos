function generateURL(){
  var year = document.getElementById('year').value;
  var month = document.getElementById('month').value;
  var day = document.getElementById('day').value;
  var hour = document.getElementById('hours').value;
  var minutes = document.getElementById('minutes').value;

  var url = "https://jugamos.patojad.com.ar/?year=";
  url = url.concat(year);
  url = url.concat("&month=");
  url = url.concat(month);
  url = url.concat("&day=");
  url = url.concat(day);
  url = url.concat("&hour=");
  url = url.concat(hour);
  url = url.concat("&minutes=");
  url = url.concat(minutes);

  var textArea = document.createElement("textarea");
  textArea.value = url;

  // Avoid scrolling to bottom
  textArea.style.top = "0";
  textArea.style.left = "0";
  textArea.style.position = "fixed";

  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';

    var toastElList = [].slice.call(document.querySelectorAll('.toast'))
    var toastList = toastElList.map(function(toastEl) {
      return new bootstrap.Toast(toastEl, {}).show()
    })

    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
